package com.example.dicodingprojectfix

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.example.latihan1.homeActivity.HomeFragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.appbar.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.home_activity.*
import kotlinx.android.synthetic.main.home_header.*

class NaviActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        actionbar()
        displayScreen(-1)


    }

    private fun actionbar() {
        val navBar = ActionBarDrawerToggle(
            Activity(),
            drawer_layout,
            toolbar_rcy,
            R.string.nav_open,
            R.string.nav_close
        )
        drawer_layout.addDrawerListener(navBar)
        navBar.syncState()
        nav_navi.setNavigationItemSelectedListener(this)

    }

    fun displayScreen(id: Int) {
        val fragment = when (id) {
            R.id.nav_profile -> {
                ProfleFragment()
            }
            R.id.nav_androver -> {
                HomeFragment()
            }
            else -> {
                HomeFragment()
            }
        }
        supportFragmentManager.beginTransaction().replace(R.id.layer, fragment).commit()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        displayScreen(item.itemId)
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


}
