package com.example.latihan1.homeActivity

import android.content.Intent
import com.example.dicodingprojectfix.R

object AndroidData {
        val androidName = arrayOf(
            "Cupcake", "Donut", "Eclair", "Frozen Yoghurt",
            "GingerBread", "Honeycomb", "Ice Cream Sandwich",
            "Jelly Bean", "KitKat", "Lolipop",
            "Marsmallow", "Nougat", "Oreo", "Pie"
        )

        val androidDetail = arrayOf(
            "Sistem operasi Android Cupcake adalah Android generasi pertama yang diluncurkan secara komersil. Sistem operasi ini juga yang pertama kalinya diberikan nama kue atau camilan manis. Sistem operasi versi paling awal ini memiliki fitur yang dibilang sudah lazim saat ini seperti widget, auto rotate, dan keyboard virtual supaya memudahkan pengguna mengetik dan kustomasi keyboard. Bisa dibilang Android Cupcake hanya memiliki fitur Android dasar saja.",
            "Setelah Cupcake, Google memberi nama Donut pada versi sistem operasi Android terbarunya. Di versi ini, Google sudah menyematkan fitur canggih seperti mesin penejermah teks ke suara. Selain itu ada juga dukungan CDMA dan indikator penggunaan baterai. Secara umum, update yang ada di Android Donut hanya sekedar penambahan beberapa fitur dan perbaikan bug saja.",
            "Android Eclair adalah versi Android yang dipersiapkan untuk menuju Hp pintar masa depan. Sistem operasi ini sudah dibekali dukungan fitur kamera seperti: flash, fokus, hingga efek warna. Selain itu, dari segi tampilan sistem operasi ini juga nampak segar dengan tambahan fitur Live Wallpaper yang bisa dibilang kerena pada masanya.",
            "Mengingat popularitas Android yang semakin berkembang pesat, Google kembali merilis banyak fitur baru di versi Android terbarunya yaitu Froyo atau singkatan dari Frozen Yogurt. Versi Android ini punya performa dan optimalisasi yang cukup signifikan. Tersedia pula fitur yang cukup inovatif pada saat itu, seperti USB tethering, portabel WiFi Hotspot, dan push notification. Di versi ini, pengguna juga bisa memindahkan data-data aplikasi besar ke memori eksternal.",
            "Bisa dibilang, Android Gingerbread adalah puncak dari popularitas Android. Versi ini cukup banyak menyedot perhatian publik dunia yang sebelumnya lebih banyak memiliki perangkat iOS, Blackberry, atau masih memakai feature phone. Pada Gingerbread, tampilan Android dibuat lebih simpel dan intuitif. Selain itu, juga ada fitur NFC untuk sistem pembayaran modern.",
            "Tak banyak yang tahu mengenai kehadiran Android versi Honeycomb. Ini disebabkan karena sistem operasi ini hanya dikhususkan untuk perangkat tablet saja. Sistem operasi Honeycomb memiliki beberapa fitur unggulan di antaranya System Bar, dukungan processor multi-core, dan layar home atau home screen yang bisa dikustomisasi dengan bebas.",
            "Setelah meluncurkan Honeycomb, Google merasa merilis sistem operasi Android yang terpisah antara Hp dan tablet cukup merepotkan. Oleh karena itu, melalui Ice Cream Sandwich Google kembali menyatukan sistem operasi Android di Hp dan tablet menjadi satu. Karena digabung jadi satu kembali, untuk mengoptimasi sistem operasi akan jadi lebih mudah. Android Ice Cream Sandwich atau disingkat ICS memiliki fitur unggulan Android Beam yang memungkinkan pengguna bisa transfer data dengan cepat melalui NFC.",
            "Tak banyak fitur baru yang ditawarkan pada Android Jelly Bean dibanding pendahulunya (ICS). Google hanya fokus untuk meningkatkan performa dan keamanan sistem operasi Android pada versi ini. Jikapun ada fitur baru yang ada di Jelly Bean adalah dukungan konten gambar 4K UHD dan dukungan emoji.",
            "Sistem operasi Android KitKat membawa banyak peningkatan yang cukup signifikan. Pada versi ini, Google fokus meningkatkan user experience pengguna Android dengan meningkatkan performa sistem operasinya. Sebagai konsekuensi dari peningkatan user experience ini adalah user tidak bisa memindahkan data-data aplikasi ke memori eksternal. Ini dikarenakan untuk mendapatkan performa aplikasi yang lebih kencang semua data yang dibutuhkan harus ada di dalam memori internal.",
            "Sempat banyak yang berharap Google dan Android membuat tampilan antarmuka (UI) lebih baru dan segar di versi Android terbarunya. Namun sayang saat versi Lollipop diluncurkan, banyak pengguna yang melihat tidak ada perubahan berarti dari segi tampilan. Tampilan UI Android Lollipop justru dianggap membosankan. Jika ada fitur baru yang hadir dan paling mencolok hanyalah kemampuan untuk melihat notifikasi saat layar terkunci (lockscreen).",
            "Untuk menyudahi kekecawaaan pengguna Android terhadap Lollipop, Android kembali merilis sistem operasi baru yaitu Marshmallow. Sistem operasi ini membawa banyak fitur baru yang lebih canggih. Beberapa fitur baru yang ada di Marshmallow di antaranya adalah: Doze untuk menghemat baterai, dukungan sensor sidik jari untuk buka kunci layar, dukungan USB tipe C, dan fitur percobaan Multi-Window agar penggunanya bisa memakai 2 aplikasi berbeda dalam 1 layar.",
            "Sistem operasi Nougat adalah pengembangan dari Marshmallow. Versi Android ini membawa peningkatan performa dan antarmuka yang lebih intuitif. Selain itu, fitur Multi-Window yang masih dalam tahap uji coba sebelumnya kini sudah sempurna dan mendukung lebih banyak aplikasi. Dengan fitur ini, pengguna bisa pakai 2 aplikasi bersamaan, misalnya nonton YouTube sambil balas pesan WhatsApp.",
            "Tampilan Android Oreo sangat berbeda dengan pendahulunya. Tampilan UI di versi Oreo kini lebih rapi dan segar. Tampilannya pun lebih difokuskan untuk memudahkan pengguna mengakses aplikasi dan mencari informasi yang dibutuhkan. Tersedia juga fitur baru seperti Notification Dots, Picture in Picture, Autofill untuk memudahkan isi formulir online, emoji, dan masih banyak lagi.",
            "Versi Android terbaru dengan nama kue Pie membawa lompatan baru dalam sejarah sistem operasi Hp. Android Pie sudah didukung kemampuan kecerdasan buatan (AI). Berkat AI, Hp Android jadi semakin pintar karena bisa mempelajari pola penggunaan secara otomatis. Android Pie bisa menyesuaikan konsumsi baterai dengan rutinitasmu sehari-hari. Aplikasi yang ditampilkan di halaman utama pun bisa disesuaikan dengan aplikasi apa yang paling sering digunakan dan bermanfaat saat menjalankan aktivitas sehari-hari. Android Pie juga bisa menganalisa waktu pemakaian Hp dan mengingatkanmu jika terlalu lama menggunakan Hp."
        )

        val androidPicture = intArrayOf(
           R.drawable.cupcake,
            R.drawable.donut,
            R.drawable.eclair,
            R.drawable.froyo,
            R.drawable.gingerbread,
            R.drawable.honeycomb,
            R.drawable.icecreamsandwich,
            R.drawable.jellybean,
            R.drawable.kitkat,
            R.drawable.lolipop,
            R.drawable.marsmallow,
            R.drawable.nougat,
            R.drawable.oreo,
            R.drawable.pie
        )
    }
