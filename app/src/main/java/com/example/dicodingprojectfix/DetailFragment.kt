package com.example.dicodingprojectfix

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.detail_fragment.*


class DetailFragment : Fragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.detail_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val andronamee = arguments?.getString("androidname")
        val androdetaill = arguments?.getString("androiddetail")
        val andropicturee = arguments?.getInt("andropicture")

        tv_detailname.text = andronamee
        tv_detail2.text = androdetaill
        andropicturee?.let { img_detail.setImageResource(it) }


    }

}